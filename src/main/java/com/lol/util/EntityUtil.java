package com.lol.util;

import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.UUID;

public class EntityUtil {


    public static<T> T createEntity(T entity) {
        Session session = DAO.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(entity);
        session.getTransaction().commit();
        session.close();
        return entity;
    }

    public static<T> void removeEntity(Class<T> tClass, UUID id) {
        Session session = DAO.getSessionFactory().openSession();
        session.beginTransaction();
        T entity = session.get(tClass, id);
        session.delete(entity);
        session.getTransaction().commit();
        session.close();
    }

    public static<T> T editEntity(T entity) {
        Session session = DAO.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(entity);
        session.getTransaction().commit();
        session.close();
        return entity;
    }

    public static<T> List<T> getEntities(Class<T> tClass) {
        Session session = DAO.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("from " + tClass.getName() + " ");
        List<T> entityList = query.list();
        session.close();
        return entityList;
    }

    public static<T> T getEntity(Class<T> tClass, UUID id) {
        Session session = DAO.getSessionFactory().openSession();
        session.beginTransaction();
        T entity = session.get(tClass, id);
        session.close();
        return entity;
    }
}
