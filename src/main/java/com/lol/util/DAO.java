package com.lol.util;

import com.lol.entities.UserEntity;
import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;

import java.util.List;

public class DAO {

    private static SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            if (sessionFactory == null) {
                sessionFactory = new Configuration().configure().buildSessionFactory();
            }
            return sessionFactory;
        } catch (Throwable e) {
            System.err.println("Initial SessionFactory creation failed." + e);
            throw new ExceptionInInitializerError();
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        sessionFactory.close();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        shutdown();
    }

    public static void main(String[] agrs) {
//        UserEntity userEntity = new UserEntity();
//        userEntity.setFirstName("fsdf");
//        userEntity.setSecondName("hhgff");
//        UserEntity a = EntityUtil.createEntity(userEntity);
        List<UserEntity> userEntities = EntityUtil.getEntities(UserEntity.class);
        userEntities.forEach(userEntity -> System.out.println(userEntities));

    }
}
