<%@ page import="com.lol.entities.UserEntity" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: посохаев
  Date: 07.01.2020
  Time: 19:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
<% List<UserEntity> users = (List<UserEntity>) request.getAttribute("users"); %>
<table>
    <tr>
        <th>First Name</th>
        <th>Second Name</th>
    </tr>
    <c:forEach items = "${users}" var = "user" varStatus = "status">
        <tr>${user.firstName}</tr>
        <tr>${user.secondName}</tr>
    </c:forEach>
</table>
</body>
</html>
